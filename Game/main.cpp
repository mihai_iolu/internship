#include<iostream>
using namespace std;

#include "GameAPI.h"

int main(int argc, char** argv)
{
	auto g = IGame::Produce();

	if (!g)
		return -1;

	if (!g->Init())
		return -1;

	g->DoSomething();
	g->DoSomething();

	g->SetStrategy(EStrategy::MEDIUM);

	g->DoSomething();
	g->DoSomething();

	return 0;
}