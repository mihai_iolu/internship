#pragma once

#include "API/GameAPI.h"

class EasyStrategy : public IStrategy
{
public:
	void MakeMove() override;
};

class MediumStrategy : public IStrategy
{
public:
	void MakeMove() override;
};

class Game : public IGame
{
public:
	Game();

	Game(const Game&) = delete;
	Game& operator=(const Game&) = delete;

	bool Init() override;

	void DoSomething() override;

	void SetStrategy(EStrategy strategyType) override;
	void SetStrategy(shared_ptr<IStrategy> newStrategy) override;

private:
	shared_ptr<IStrategy> m_strategy;
};
