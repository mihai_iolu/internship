#pragma once

#include <memory>

enum class EStrategy
{
	EASY,
	MEDIUM
};

class IStrategy
{
public:
	virtual ~IStrategy() = default;

	virtual void MakeMove() = 0;
};

class IGame
{
public:
	using Ptr = std::shared_ptr<IGame>;

	virtual ~IGame() = default;

	virtual bool Init() = 0;

	virtual void DoSomething() = 0;

	virtual void SetStrategy(EStrategy strategyType) = 0;
	virtual void SetStrategy(std::shared_ptr<IStrategy> newStrategy) = 0;

	static IGame::Ptr Produce();
};
