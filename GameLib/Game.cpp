#include <iostream>
using namespace std;

#include "Game.h"

void EasyStrategy::MakeMove()
{
	cout << "EasyStrategy::MakeMove()" << endl;
}

void MediumStrategy::MakeMove()
{
	cout << "MediumStrategy::MakeMove()" << endl;
}

IGame::Ptr IGame::Produce()
{
	return make_shared<Game>();
}

Game::Game() 
	: m_strategy(new EasyStrategy())
{

}

bool Game::Init()
{
	return true;
}

void Game::DoSomething()
{
	m_strategy->MakeMove();
}

void Game::SetStrategy(EStrategy strategyType)
{
	switch (strategyType)
	{
	case EStrategy::EASY:
		m_strategy = make_shared<EasyStrategy>();
		break;
	case EStrategy::MEDIUM:
		m_strategy = make_shared<MediumStrategy>();
		break;
	default:
		break;

	}
}

void Game::SetStrategy(shared_ptr<IStrategy> newStrategy)
{
	m_strategy = newStrategy;
}
