#include <gtest/gtest.h>
#include "BaseEnvironment.h"

#include <iostream>
using namespace std;

extern ::testing::Environment* env;

class TestSuite1 : public ::testing::Test
{
protected:
	int x, y;
	int* a = new int[10];

	void SetUp()
	{
		x = 10;
		y = 100;
		cout << "TestSuite1::SetUp" << endl;
	}

	void TearDown()
	{
		cout << "TestSuite1::TearDown" << endl;
		delete[] a;
	}

	void test()
	{
		BaseEnvironment* my_env = (BaseEnvironment*)env;
	}
};

TEST_F(TestSuite1, Test1)
{
	x = 200;
	ASSERT_TRUE(1);
}

TEST_F(TestSuite1, Test2)
{
	ASSERT_TRUE(1);
}